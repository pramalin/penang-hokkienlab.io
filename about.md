---
layout: default
title: About
permalink: /about/
css: about
---
<main class="l-content c-content">
  <section class="l-about">
    <div class="ms-wrapper">
      <header class="l-header c-header">
        <h1 class="l-title c-title">
          <span class="l-title__zh c-title__zh" lang="zh">关于</span>
          <span class="l-title__en c-title__en">About</span>
        </h1>
      </header>
      <div class="l-wrapper">
        {% include language.html %}
        <div class="l-lang__en c-lang__en">
          <p>This is a website dedicated to Penang Hokkien, the language of my hometown. I talk about the inspiration for the content of this site in the <a href="{{ "/stories/01" | prepend: site.baseurl }}">inaugural story post</a>. But I am also a web developer, and so this site also serves as my CSS playground for experimenting with the concept of mixed writing-modes. If that sentence made sense to you, <a href="https://gitlab.com/penang-hokkien/penang-hokkien.gitlab.io">technical details here</a>.</p>
        </div>
        <div class="l-lang__zh c-lang__zh" lang="zh">
          <p>庇能福建是我家乡的语言，是槟城文化的栋梁之一。网站的由来已经在<a href="{{ "/stories/01" | prepend: site.baseurl }}">「做么要创建这个网站呢？」</a>里描述了。</p>
          <p>我也是一名网络开发者，而这个网站让我试用另类的网络设计，尝试用<span class="u-txt-upright">CSS</span>（层叠样式表）混合水平书写方式及垂直书写方式。如果听懂那个句子，可以<a href="https://gitlab.com/penang-hokkien/penang-hokkien.gitlab.io">点击这里了解更多详情</a>。</p>
        </div>
      </div>
    </div>
  </section>
  {% include newsletter.html %}
  {% include home-link.html %}
</main>
